<?php
namespace Crawler\MeetupBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MDB;

/**
 * @MDB\Document
 * @MDB\Indexes({
 *   @MDB\Index(keys={"group_name"="asc"})
 * })
 */
class Group
{
    //using group url as id for now, since certain group titles are coming through
    //as empty strings when using utf-8 encoding    
    /**
     * @MDB\Id(strategy="NONE")
     */
    protected $id;
    
    /**
     * @MDB\String
     */
    protected $group_title;    

    /**
     * @MDB\Field(type="string")
     */
    protected $location;

    /**
     * @MDB\Field(type="string")
     */
    protected $url;    

    /**
     * @MDB\Field(type="int")
     */
    protected $member_count;

	/**
     * @MDB\Field(type="date")
     */
	protected $created;
	
	/**
     * @MDB\Field(type="date")
     */
	protected $modified;
	    
    /**
     * @MDB\Field(type="float")
     * 
     * This is the distance measured from 90010,
     * after translated into lat/lng by Google Geocoder
     */
    protected $distance_a;
    
    /**
     * @MDB\ReferenceMany(targetDocument="Group_X_Topic", mappedBy="group")
     */
    protected $topics;
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group_title
     *
     * @param string $groupTitle
     */
    public function setGroupTitle($groupTitle)
    {
        $this->group_title = $groupTitle;
    }

    /**
     * Get group_title
     *
     * @return string $groupTitle
     */
    public function getGroupTitle()
    {
        return $this->group_title;
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set member_count
     *
     * @param int $memberCount
     */
    public function setMemberCount($memberCount)
    {
        $this->member_count = $memberCount;
    }

    /**
     * Get member_count
     *
     * @return int $memberCount
     */
    public function getMemberCount()
    {
        return $this->member_count;
    }
    
    public function __construct()
    {
        $this->topics = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add topics
     *
     * @param Crawler\MeetupBundle\Document\Group_X_Topic $topics
     */
    public function addTopics(\Crawler\MeetupBundle\Document\Group_X_Topic $topics)
    {
        $this->topics[] = $topics;
    }

    /**
     * Get topics
     *
     * @return Doctrine\Common\Collections\Collection $topics
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Set distance_a
     *
     * @param float $distanceA
     */
    public function setDistanceA($distanceA)
    {
        $this->distance_a = $distanceA;
    }

    /**
     * Get distance_a
     *
     * @return float $distanceA
     */
    public function getDistanceA()
    {
        return $this->distance_a;
    }

    /**
     * Set created
     *
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param date $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * Get modified
     *
     * @return date $modified
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set group_name
     *
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->group_name = $groupName;
    }

    /**
     * Get group_name
     *
     * @return string $groupName
     */
    public function getGroupName()
    {
        return $this->group_name;
    }

    /**
     * Set id
     *
     * @param custom_id $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
