<?php
namespace Crawler\MeetupBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MDB;

/**
 * @MDB\Document(
 * collection="topics_groups"
 * )
 * @MDB\Indexes({
 *   @MDB\Index(keys={"group_title"="asc"})
 * })
 */
class Group_X_Topic
{
    /**
     * @MDB\Id
     */
    protected $id;

    /**
     * @MDB\ReferenceOne(targetDocument="Group", inversedBy="topics") 
     */
    protected $group; 
    
    /**
     * @MDB\Field(type="string")
     */
    protected $group_name;
    
    /**
     * @MDB\Field(type="string")
     */
    protected $topic;

	/**
     * @MDB\Field(type="date")
     */
	protected $created;
	
	/**
     * @MDB\Field(type="date")
     */
	protected $modified;
	
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set topic
     *
     * @param string $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * Get topic
     *
     * @return string $topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set group
     *
     * @param Crawler\MeetupBundle\Document\Group $group
     */
    public function setGroup(\Crawler\MeetupBundle\Document\Group $group)
    {
        $this->group = $group;
    }

    /**
     * Get group
     *
     * @return Crawler\MeetupBundle\Document\Group $group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set created
     *
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param date $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * Get modified
     *
     * @return date $modified
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set group_title
     *
     * @param string $groupTitle
     */
    public function setGroupTitle($groupTitle)
    {
        $this->group_title = $groupTitle;
    }

    /**
     * Get group_title
     *
     * @return string $groupTitle
     */
    public function getGroupTitle()
    {
        return $this->group_title;
    }

    /**
     * Set group_name
     *
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->group_name = $groupName;
    }

    /**
     * Get group_name
     *
     * @return string $groupName
     */
    public function getGroupName()
    {
        return $this->group_name;
    }
}
