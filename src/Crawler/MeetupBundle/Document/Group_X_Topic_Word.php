<?php
namespace Crawler\MeetupBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MDB;

/**
 * @MDB\Document(collection="GroupTopicWord_1")
 */
class Group_X_Topic_Word
{
    /**
     * @MDB\Id
     */
    protected $id;
    
    /**
     * @MDB\Field(type="string")
     * @MDB\Index
     */
    protected $word;
    
    /**
     * @MDB\Field(type="int")
     */
    protected $avgMembers;
    
    /**
     * @MDB\Field(type="int")
     */
    protected $frequency;    

	/**
     * @MDB\Field(type="date")
     */
	protected $created;
	
	/**
     * @MDB\Field(type="date")
     */
	protected $modified;	

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word
     *
     * @param string $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }

    /**
     * Get word
     *
     * @return string $word
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set avgMembers
     *
     * @param int $avgMembers
     */
    public function setAvgMembers($avgMembers)
    {
        $this->avgMembers = $avgMembers;
    }

    /**
     * Get avgMembers
     *
     * @return int $avgMembers
     */
    public function getAvgMembers()
    {
        return $this->avgMembers;
    }

    /**
     * Set frequency
     *
     * @param int $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * Get frequency
     *
     * @return int $frequency
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set created
     *
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param date $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * Get modified
     *
     * @return date $modified
     */
    public function getModified()
    {
        return $this->modified;
    }
}
