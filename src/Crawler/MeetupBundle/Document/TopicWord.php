<?php
namespace Crawler\MeetupBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MDB;

/**
 * @MDB\Document(collection="TopicWord_1")
 * @MDB\Indexes({
 *   @MDB\Index(keys={"frequency"="asc"}),
 *   @MDB\Index(keys={"avg_members"="asc"}),
 *   @MDB\Index(keys={"group_title"="asc"})
 * }) 
 */
class TopicWord
{    
    /** 
    * @MDB\Id(strategy="NONE")
    */
    private $id;
    
    /**
     * @MDB\Field(type="string")
     */
    protected $group_title;
    
    /**
     * @MDB\Field(type="int")
     */
    protected $avg_members;
    
    /**
     * @MDB\Field(type="int")
     */
    protected $frequency;    

	/**
     * @MDB\Field(type="date")
     */
	protected $created;
	
	/**
     * @MDB\Field(type="date")
     */
	protected $modified;

    /**
     * Set group_title
     *
     * @param string $groupTitle
     */
    public function setGroupTitle($groupTitle)
    {
        $this->group_title = $groupTitle;
    }

    /**
     * Get group_title
     *
     * @return string $groupTitle
     */
    public function getGroupTitle()
    {
        return $this->group_title;
    }

    /**
     * Set avg_members
     *
     * @param int $avgMembers
     */
    public function setAvgMembers($avgMembers)
    {
        $this->avg_members = $avgMembers;
    }

    /**
     * Get avg_members
     *
     * @return int $avgMembers
     */
    public function getAvgMembers()
    {
        return $this->avg_members;
    }

    /**
     * Set frequency
     *
     * @param int $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * Get frequency
     *
     * @return int $frequency
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set created
     *
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param date $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * Get modified
     *
     * @return date $modified
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set id
     *
     * @param custom_id $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return custom_id $id
     */
    public function getId()
    {
        return $this->id;
    }
}
