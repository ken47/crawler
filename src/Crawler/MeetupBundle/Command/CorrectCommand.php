<?php
namespace Crawler\MeetupBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand,
        Symfony\Component\Console\Input\InputArgument,
        Symfony\Component\Console\Input\InputInterface,
        Symfony\Component\Console\Input\InputOption,
        Symfony\Component\Console\Output\OutputInterface,
        Crawler\MeetupBundle\Document\Group, //model
        Crawler\MeetupBundle\Document\Group_X_Topic, //model
        Symfony\Component\HttpFoundation\Response;

class CorrectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('meetup:correct')
            ->setDescription('Meetup Correct')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        ini_set('memory_limit', '3000M');
        $this->output =& $output;
        
        $startTime = time();
        $this->emailLog = '';
        $this->log('COMMENCING ANALYSIS', true);
        $this->log("==========================", true);
        $DoctrineDocumentManager = $this->getContainer()->get('doctrine.odm.mongodb.default_document_manager');        
        $DoctrineDocumentManager->getSchemaManager()->ensureIndexes();
        
        $this->analyzeByLocation($DoctrineDocumentManager);
        $this->analyzeByWord($DoctrineDocumentManager);
        
        $endTime = time();
        $duration = ($endTime - $startTime) / 60;
        $this->log("\n\nANALYSIS ENDED AFTER {$duration} MINUTES", true);
        $this->emailToAdmin($this->emailLog);
    }
    
    protected function getTopicWordDoc($word, $groupTitle)
    {
        $Group_X_Title_Word_Doc = $dm->getRepository('CrawlerMeetupBundle:Group_X_Topic_Word')
            ->findOneBy(array('group_title' => $groupTitle, 'word' => $word));
            
        $persist = false; //doctrine persistence required for new document
        
        if (!$Group_X_Topic_Word) {
            $Group_X_Topic_Word = new Group_X_Topic_Word();
            $persist = true;
        }
        
        return array(
            'document' => $Group_X_Topic_Word,
            'persist' => $persist
        );
    }
    
    protected function getTitleWordDoc($word, $groupTitle)
    {
        $Group_X_Title_Word_Doc = $dm->getRepository('CrawlerMeetupBundle:Group_X_Title_Word')
            ->findOneBy(array('group_title' => $groupTitle, 'word' => $word));
            
        $persist = false; //doctrine persistence required for new document
        
        if (!$Group_X_Title_Word) {
            $Group_X_Title_Word = new Group_X_Title_Word();
            $persist = true;
        }
        
        return array(
            'document' => $Group_X_Title_Word,
            'persist' => $persist
        );
    }
    
    protected function analyzeByWord(\Doctrine\ODM\MongoDB\DocumentManager &$dm)
    {
        $this->log("COMMENCED TOPIC WORD ANALYSIS");
        $Groups = $dm->createQueryBuilder('CrawlerMeetupBundle:Group')
            ->sort('group_title', 'asc')->limit(100000)
            ->getQuery()->execute();
            
        $wordSort = array(
            'topic' => array(
                'count' => array(),
                'avgMembers' => array()
            ),
            'title' => array(
                'count' => array(),
                'avgMembers' => array()
            )
        );
                
        $countGroups = count($Groups);
        $groupCounter = 0;
        
        try {
            foreach($Groups as $k=>$Group) {
                $groupCounter++;
                
                $progressPercent = round($groupCounter / $countGroups, 2);
                $this->log("ANALYZING NEW GROUP {$groupCounter}/{$countGroups} ({$progressPercent}%)");
                $groupTitle = $Group->getGroupTitle();
                $titleArray = $this->splitWords($groupTitle);
                if(empty($titleArray))
                    break;
                foreach($titleArray as $word) {
                    $Group_X_Title_Word_Doc = $this->getTitleWordDoc($word, $groupTitle);
                    $word = trim($word);
                    if(empty($wordSort['title']['avgMembers'][$word])) {
                        $wordSort['title']['avgMembers'][$word] = $Group->getMemberCount();
                        $wordSort['title']['count'][$word] = 1;
                    } else {
                        $wordSort['title']['avgMembers'][$word] = 
                            (($wordSort['title']['avgMembers'][$word] * $wordSort['title']['count'][$word]) + $Group->getMemberCount())
                            / ($wordSort['title']['count'][$word] + 1);
                        $wordSort['title']['count'][$word]++;    
                    }
                }
                
                
                $Topics = $Group->getTopics();
                foreach($Topics as $Topic) {
                    $topicText = $Topic->getTopic();
                    $wordsArray = $this->splitWords($topicText);
                    if(empty($wordsArray))
                        break;
                    foreach($wordsArray as $word) {
                        $word = trim($word);
                        $Group_X_Topic_Word_Doc = $this->getTopicWordDoc($word, $groupTitle);
                        
                        if(empty($wordSort['topic']['avgMembers'][$word])) {
                            $wordSort['topic']['avgMembers'][$word] = $Group->getMemberCount();
                            $wordSort['topic']['count'][$word] = 1;
                        } else {
                            $wordSort['topic']['avgMembers'][$word] = 
                                (($wordSort['topic']['avgMembers'][$word] * $wordSort['topic']['count'][$word]) + $Group->getMemberCount())
                                / ($wordSort['topic']['count'][$word] + 1);
                            $wordSort['topic']['count'][$word]++;    
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->log("EXCEPTION: " . $e->getMessage(), true);
        }
        arsort($wordSort['title']['avgMembers']);
        $count = 0;
        foreach($wordSort['title']['avgMembers'] as $word=>$stats) {
            if($count == 50)
                break;
            else
                $count++;
            /*
            $this->log("\n\nTITLE WORD ANALYSIS", true);
            $this->log("==========================", true);
            $this->log("{$word}: {$stats} avg members", true);
            */
        }
        
        arsort($wordSort['topic']['avgMembers']);
        $count = 0;
        foreach($wordSort['topic']['avgMembers'] as $word=>$stats) {
            if($count == 50)
                break;
            else
                $count++;
            /*
            $this->log("\n\nTOPIC WORD ANALYSIS", true);
            $this->log("==========================", true);
            $this->log("{$word}: {$stats} avg members", true);
            */
        }
    }
    
    protected function analyzeByLocation(\Doctrine\ODM\MongoDB\DocumentManager &$DoctrineDocumentManager)
    {
        $qb = $dm->createQueryBuilder('CrawlerMeetupBundle:Group');
        $query = $qb->map('function() { emit(this.location, 1); }')
            ->reduce('function(k,v) {
                var count = 0;
                for (var i in v) {
                    count += v[i];
                };
                return count;
            }')            
            ->getQuery();
        $results = $query->execute();
        
        $sort = array();
        foreach($results as $k=>$result) {
            $sort[$k] = $result['value'];
        }
        
        arsort($sort);

        $final = array();
        $count = 0;
        
        $this->log("CALCULATING AVG MEMBERSHIP BY LOCATION");
        foreach($sort as $k=>$dummy) {            
            $GroupsByLocation = $dm->getRepository('CrawlerMeetupBundle:Group')->findBy(array('location' => $results[$k]['_id']));
            
            $total = 0;            
            foreach($GroupsByLocation as $Group) {
                $total += $Group->getMemberCount();
            }            
            $avgMembership = $total / count($GroupsByLocation);
            $final[$results[$k]['_id']] = array(
                'count' => $results[$k]['value'],
                'avgMembers' => $avgMembership
            );
            if(++$count == 20)
                break;
        }
        
        $this->log("\n\nSTATS BY LOCATION", true);
        $this->log("==========================", true);
        foreach($final as $locationName => $stats) {
            $this->log("{$locationName}: {$stats['count']} groups, {$stats['avgMembers']} avg membership", true);
        }
    }
    
    protected function emailToAdmin($bodyText)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Crawl Ended @' . time())
            ->setFrom('kenhahn85t@gmail.com')
            ->setTo('kenhahn85@gmail.com')
            ->setBody($bodyText);
            
        $this->getContainer()->get('mailer')->send($message);
    }
    
    protected function log($message, $email = false)
    {
        $this->output->writeln(date('d M Y (H:i:s)', time()) . " - {$message}\n");
        if ($email) {
            $this->emailLog .= $message . "\n";
        }
    }
    
    protected function splitWords($text)
    {   
        $echo = false;
        if (strtolower(trim($text)) == "executive and business coaching") $echo = true;
        $text = preg_replace('/( -)/', '', $text);
        $text = preg_replace('/(- )/', '', $text);
        $text = preg_replace('/[^a-zA-Z0-9-\s\']/', '', $text);
        $commonWords = array( //can expand later
            ' and ',
            ' or ',
            ' if ',
            ' of ',
            ' the ',
            ' with ',
            ' where ',
            ' in '
        );
        $text = str_ireplace($commonWords, '', $text);
        $textArray = explode(' ', $text);
        return $textArray;
    }
    
    protected function compilePageStats(&$groups)
    {
        $this->log("COMPILING PAGE STATS");
        
        $this->stats['totalGroups'] += count($groups);
        
        $groupCount = 0;
        foreach($groups as $group) {
            $groupCount++;
            
            if (empty($this->stats['avgMembers']['_all'])) {
                $this->stats['avgMembers']['_all'] = $group['MemberCount'];
                $this->stats['frequency']['_all'] = 1;
            } else {
                $this->stats['avgMembers']['_all'] = 
                    (($this->stats['avgMembers']['_all'] *  $this->stats['frequency']['_all']) + $group['MemberCount']) / 
                    ($this->stats['frequency']['_all'] + 1);
                $this->stats['frequency']['_all']++;
            }

            $titleWords = $this->splitWords($group['GroupTitle']);
            foreach($titleWords as $word) {
                if (empty($this->stats['avgMembers']['byTitleWord'][$word])) {
                    $this->stats['avgMembers']['byTitleWord'][$word] = $group['MemberCount'];
                    $this->stats['frequency']['byTitleWord'][$word] = 1;
                } else {
                    $this->stats['avgMembers']['byTitleWord'][$word] = 
                        (($this->stats['avgMembers']['byTitleWord'][$word] *  $this->stats['frequency']['byTitleWord'][$word]) + $group['MemberCount']) / 
                        ($this->stats['frequency']['byTitleWord'][$word] + 1);
                    $this->stats['frequency']['byTitleWord'][$word]++;
                }
            }
            
            foreach($group['Topics'] as $topic) {
                if (empty($this->stats['avgMembers']['byTopic'][$topic])) {
                    $this->stats['avgMembers']['byTopic'][$topic] = $group['MemberCount'];
                    $this->stats['frequency']['byTopic'][$topic] = 1;
                } else {
                    $this->stats['avgMembers']['byTopic'][$topic] = 
                        (($this->stats['avgMembers']['byTopic'][$topic] *  $this->stats['frequency']['byTopic'][$topic]) + $group['MemberCount']) / 
                        ($this->stats['frequency']['byTopic'][$topic] + 1);
                    $this->stats['frequency']['byTopic'][$topic]++;
                }
                
                $topicWords = $this->splitWords($topic);
                foreach($topicWords as $word) {
                    if (empty($this->stats['avgMembers']['byTopicWord'][$word])) {
                        $this->stats['avgMembers']['byTopicWord'][$word] = $group['MemberCount'];
                        $this->stats['frequency']['byTopicWord'][$word] = 1;
                    } else {
                        $this->stats['avgMembers']['byTopicWord'][$word] = 
                            (($this->stats['avgMembers']['byTopicWord'][$word] *  $this->stats['frequency']['byTopicWord'][$word]) + $group['MemberCount']) / 
                            ($this->stats['frequency']['byTopicWord'][$word] + 1);
                        $this->stats['frequency']['byTopicWord'][$word]++;
                    }
                }
            }
        }
        
        return $groupCount;
    }
    
    public function aggregateStats()
    {
        $this->log("AGGREGATING AND SORTING STATS");
        
        $printOut = "Summary for results within {$this->radius} miles of {$this->zip}. \n";
        $printOut .= "Execution Time: {$this->scriptDuration} seconds\n\n";
        $printOut .= "=====================================\n";
        $printOut .= "Average Membership for All Groups in Location\n";
        $printOut .= "=====================================\n";
        $printOut .= "{$this->stats['avgMembers']['_all']}\n";
        
        $printOut .= "\nQty of Groups in Location\n";
        $printOut .= "=====================================\n";
        $printOut .= "{$this->stats['frequency']['_all']}\n";
        $this->log($printOut, true);
        
        
        arsort($this->stats['avgMembers']['byTopic']);        
        $count = 0;       
        $printOut = "\nTop " . self::MAX_RESULTS . ": Average Members by Topic\n";
        $printOut .= "=====================================\n";
        foreach($this->stats['avgMembers']['byTopic'] as $topic=>$avgMembers) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$avgMembers} members (Frequency {$this->stats['frequency']['byTopic'][$topic]})\n";
        }
        $this->log($printOut, true);
        
        
        arsort($this->stats['frequency']['byTopic']);
        $count = 0;
        $printOut = "\nTop " . self::MAX_RESULTS . ": Frequency by Topic\n";
        $printOut .= "=====================================\n";
        
        foreach($this->stats['frequency']['byTopic'] as $topic=>$frequency) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$frequency} frequency (Avg Members {$this->stats['avgMembers']['byTopic'][$topic]})\n";
        }
        $this->log($printOut, true);

        
        arsort($this->stats['avgMembers']['byTopicWord']);        
        $count = 0;       
        $printOut = "\nTop " . self::MAX_RESULTS . ": Average Members by Topic Word\n";
        $printOut .= "=====================================\n";
        foreach($this->stats['avgMembers']['byTopicWord'] as $topic=>$avgMembers) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$avgMembers} members (Frequency {$this->stats['frequency']['byTopicWord'][$topic]})\n";
        }
        $this->log($printOut, true);
        
        
        arsort($this->stats['frequency']['byTopicWord']);
        $count = 0;
        $printOut = "\nTop " . self::MAX_RESULTS . ": Frequency by Topic Word\n";
        $printOut .= "=====================================\n";
        
        foreach($this->stats['frequency']['byTopicWord'] as $topic=>$frequency) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$frequency} frequency (Avg Members {$this->stats['avgMembers']['byTopicWord'][$topic]})\n";
        }
        $this->log($printOut, true);
        
        
        arsort($this->stats['avgMembers']['byTitleWord']);        
        $count = 0;       
        $printOut = "\nTop " . self::MAX_RESULTS . ": Average Members by Title Word\n";
        $printOut .= "=====================================\n";
        foreach($this->stats['avgMembers']['byTitleWord'] as $topic=>$avgMembers) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$avgMembers} members (Frequency {$this->stats['frequency']['byTitleWord'][$topic]})\n";
        }
        $this->log($printOut, true);
        
        
        arsort($this->stats['frequency']['byTitleWord']);
        $count = 0;
        $printOut = "\nTop " . self::MAX_RESULTS . ": Frequency by Title Word\n";
        $printOut .= "=====================================\n";
        
        foreach($this->stats['frequency']['byTitleWord'] as $topic=>$frequency) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut .= "{$topic}: {$frequency} frequency (Avg Members {$this->stats['avgMembers']['byTitleWord'][$topic]})\n";
        }
        $this->log($printOut, true);
        
        return true;
    }    
}
?>
