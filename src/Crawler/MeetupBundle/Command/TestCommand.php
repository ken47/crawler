<?php
// src/Crawler/GrubBundle/Command/LoginCommand.php
namespace Crawler\MeetupBundle\Command;

include_once '/../libs/simplehtmldom/SimpleHtmlDom.php';

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand,
        Symfony\Component\Console\Input\InputArgument,
        Symfony\Component\Console\Input\InputInterface,
        Symfony\Component\Console\Input\InputOption,
        Symfony\Component\Console\Output\OutputInterface,
        Symfony\Component\HttpFoundation\Response,
        SimpleHtmlDom\SimpleHtmlDom,
        Crawler\MeetupBundle\Document\Group, //model
        Crawler\MeetupBundle\Document\Group_X_Topic; //model

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('meetup:test')
            ->setDescription('Meetup Test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        
        $this->output->writeln("Beginning geocode test.");        
        $locationCoords = $this->geocode();
        $this->output->writeln("Ending geocode test.");
        $this->output->writeln("Beginning meetup test.");        
        $this->meetup($locationCoords);
        $this->output->writeln("Ending meetup test.");        
        $this->output->writeln("Beginning email test.");
        $this->email();
        $this->output->writeln("Ending email test.");
    }
    
    protected function meetup($locationCoords)
    {
        $curlParams = array(
            'offset' => 0, //TODO ask for offset as user-set parameter
            'long' => $locationCoords->lng,
            'lat' => $locationCoords->lat,
            'sort' => 'member_count',
            'op' => 'search',
            'psize' => 49,
            'radius' => 25.0,
            'show' => 'results',
        );
        
        $baseUrl = "http://www.meetup.com/find/";
        $curlUrl = $baseUrl . '?' . http_build_query($curlParams);
        
        $this->email($curlUrl);
        //$this->output->writeln($curlUrl);
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);        
        $result = curl_exec($ch);
        curl_close($ch);
        
        $SimpleHtmlDom = new SimpleHtmlDom();
        $SimpleHtmlDom->load($result);        
        $groups = $SimpleHtmlDom->find('.D_group');
        
        if (empty($groups)) {
            $this->output->writeln("Found no result.");
        }

        $groupCount = 0;
        $groupsData = array();
        foreach($groups as $group) {
            $groupsData[$groupCount] = array();
            
            $GroupDom = new SimpleHtmlDom;
            $GroupDom->load($group->outertext);                                
            //$this->output->writeln($groupListing->outertext);

            foreach($GroupDom->find('.D_name') as $title) {                    
                $groupsData[$groupCount]['GroupTitle'] = trim($title->plaintext);
                break;
            }             

            foreach($GroupDom->find('a') as $anchor) {                    
                $groupsData[$groupCount]['Url'] = $anchor->href;
                break;
            }

            foreach($GroupDom->find('.D_distance') as $locationInfo) {
                $location = trim($locationInfo->children(1)->plaintext);
                $location = substr($location, strpos($location, ' ') + 1);
                $groupsData[$groupCount]['Location'] = $location;
                break;
            }

            foreach($GroupDom->find('.D_members') as $membershipInfo) {
                $trimmedMemberCount = str_replace(',', '', trim($membershipInfo->children(0)->plaintext));
                $groupsData[$groupCount]['MemberCount'] = $trimmedMemberCount;
                break;
            }

            foreach($GroupDom->find('.meetup-topic') as $topic) {
                $groupsData[$groupCount]['Topics'][] = trim($topic->plaintext);
            }
            
            unset($GroupDom); //this fixed a memory leak
            $groupCount++;
        }
        
        unset($SimpleHtmlDom);
    }
    
    protected function geocode()
    {
        $baseUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=90027&sensor=true";        
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $baseUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);        
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result->results[0]->geometry->location;
    }
    
    protected function email($body = 'test email')
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Crawl Ended')
            ->setFrom('kenhahn85t@gmail.com')
            ->setTo('kenhahn85@gmail.com')
            ->setBody($body);
            
        $this->getContainer()->get('mailer')->send($message);
    }
}