<?php
// src/Crawler/GrubBundle/Command/LoginCommand.php
namespace Crawler\MeetupBundle\Command;

include_once '/../libs/simplehtmldom/SimpleHtmlDom.php';

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand,
        Symfony\Component\Console\Input\InputArgument,
        Symfony\Component\Console\Input\InputInterface,
        Symfony\Component\Console\Input\InputOption,
        Symfony\Component\Console\Output\OutputInterface,
        Symfony\Component\HttpFoundation\Response,
        SimpleHtmlDom\SimpleHtmlDom,
        Crawler\MeetupBundle\Document\Group, //model
        Crawler\MeetupBundle\Document\Group_X_Topic; //model

class CrawlCommand extends ContainerAwareCommand
{
    protected $specificLocation;
    const REQUEST_LIMIT = null; //set to null to get all results
    const MAX_RESULTS = 50; //for stat analysis
    const FLOOD_PROTECTION_BUFFER = 12;
    
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('meetup:crawl')
            ->setDescription('Meetup Crawl');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $this->groupsSaved = 0;
        $this->groupsProcessed = 0;
        $this->output = $output;
        $DoctrineDocumentManager = $this->getContainer()->get('doctrine.odm.mongodb.default_document_manager');        
        
        $this->dialog = $this->getHelperSet()->get('dialog');        
        $this->specificLocation = $this->dialog->ask($output, "\nWould you like to analyze a specific location? If no, all locations will be analyzed. [y/n]\n", 'n');
        
        while("n" !== strtolower($this->specificLocation) && "y" !== strtolower($this->specificLocation)) {
            $this->specificLocation = $this->dialog->ask($output, "Would you like to analyze a specific location? If no, all locations will be analyzed. [y/n]\n", 'n');
        }
        
        if ("n" === strtolower($this->specificLocation)) {
            $this->specificLocation = false;
        } else {
            $this->specificLocation = true;
        }
        
        if ($this->specificLocation) {                
            $this->stats = array(
                'avgMembers' => array(
                    '_all' => null,
                    'byTopic' => array(),
                    'byTopicWord' => array(),
                    'byTitleWord' => array()
                ),
                'frequency' => array(
                    '_all' => 0,
                    'byTopic' => array(),
                    'byTopicWord' => array(),
                    'byTitleWord' => array()
                ),
                'totalGroups' => 0
            );
            
            $this->log("We need the zip code and radius from that zip code. Please refer to Google Maps if necessary.\n");
            $this->zip = $this->dialog->ask(
                $output, "What zip code?\n"
            );
            
            while(!is_numeric($this->zip) || strlen($this->zip) !== 5) {
                $this->zip = $this->dialog->ask(
                    $output, "What zip code?\n"
                );            
            }
            
            $this->radius = $this->dialog->ask(
                $output, "\n\nWhat radius from {$this->zip} (in miles)?\n"
            );
            
            while(!is_numeric($this->radius)) {
                $this->radius = $this->dialog->ask(
                    $output, "\n\nWhat radius from {$this->zip}?\n"
                );
            }
            
            $this->log("PROCESSING...");        
            
            //$locationCoords = $this->geocodeLatLng($this->zip); //deprecated
            
            $offset = 43250; //TODO ask for offset as user-set parameter
            $this->meetupCurlParams = array(
                'offset' => $offset,
                //'lon' => $locationCoords->lng, //deprecated
                //'lat' => $locationCoords->lat,
                'sort' => 'member_count',
                'psize' => 50,
                'radius' => $this->radius . '.0',
                'show' => 'results',
            );
            ini_set('memory_limit', '3000M');
            $this->crawlAllPages(new SimpleHtmlDom, $DoctrineDocumentManager);
        } else {
            $offset = 0; //TODO ask for offset as user-set parameter
            $this->meetupCurlParams = array(
                'offset' => $offset,
                //'lon' => -97.0, //default location is Los Angeles
                //'lat' => 38.0,
                'sort' => 'member_count',
                'psize' => 50,
                'radius' => 29000.0, //circumference of the Earth, with a little cushion added
                'show' => 'results',
            );
            $this->crawlAllPages(new SimpleHtmlDom, $DoctrineDocumentManager);
            
            $bodyText = "Script execution now ending.\n
                {$this->groupsProcessed} groups processed.";
            
            $this->log($bodyText, true);
            $this->emailToAdmin($this->emailLog);
        }        
    }
    
    protected function log($message, $email = false)
    {
        $this->output->writeln(date('d M Y (H:i:s)', time()) . " - {$message}\n");
        if ($email) {
            $this->emailLog .= $message . "\n";
        }
    }
    
    protected function geocodeLatLng($zip)
    {
        $baseUrl = "http://maps.googleapis.com/maps/api/geocode/json";
        $geocodeParams = array(
            'address' => $zip,
            'sensor' => 'true'
        );
        $curlUrl = $baseUrl . '?' . http_build_query($geocodeParams);
        $this->emailToAdmin($curlUrl);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);        
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        
        return $result->results[0]->geometry->location;
    }        
    
    protected function emailToAdmin($bodyText)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Crawl Ended @' . time())
            ->setFrom('kenhahn85t@gmail.com')
            ->setTo('kenhahn85@gmail.com')
            ->setBody($bodyText);
            
        $this->getContainer()->get('mailer')->send($message);
    }
    
    protected function crawlAllPages(SimpleHtmlDom &$SimpleHtmlDom, \Doctrine\ODM\MongoDB\DocumentManager &$DoctrineDocumentManager) 
    {        
        $iterations = 0;
        $pageOffset = 0; //starting from the first group //TODO turn this into a user set parameter
        $this->pageNumber = 0;
        
        $scriptStart = time();
        while((self::REQUEST_LIMIT === null || $iterations < self::REQUEST_LIMIT) && empty($this->lastPage)) {
            $crawlResult = $this->crawlSinglePage($SimpleHtmlDom, $DoctrineDocumentManager);
            $iterations++;
            $this->meetupCurlParams['offset'] += $this->meetupCurlParams['psize']; //$this->stats['totalGroups'] group count is off because there seem to be
            //private groups which count towards the offset but are not displayed in the DOM
            //$this->log("FLOOD PROTECTION: SLEEPING...");
            //sleep(rand(self::FLOOD_PROTECTION_BUFFER-3, self::FLOOD_PROTECTION_BUFFER+3));
        }
        $scriptEnd = time();
        $this->scriptDuration = ($scriptEnd - $scriptStart) * 1000;
    }
    

    
    protected function crawlSinglePage(SimpleHtmlDom &$SimpleHtmlDom, \Doctrine\ODM\MongoDB\DocumentManager &$DoctrineDocumentManager) 
    {
        $this->log("CRAWLING NEW PAGE (pg {$this->pageNumber} | {$this->groupsSaved}/{$this->groupsProcessed} groups)");
        
        $curlResult = $this->meetupCurlCall();
        
        if (empty($curlResult)) {
            $this->log("Found no curl response at offset: {$pageOffset}.");
            return false;
        }
        
        $groups = $this->analyzeSinglePageDom($SimpleHtmlDom, $curlResult);
        
        if (empty($groups)) {
            $this->log("Either there are no groups left to crawl, or you have been rate limited.", true);
            return false;
        }
        
        $this->pageNumber++;
        
        $groupsProcessed = $this->savePageDom($DoctrineDocumentManager, $groups);
        unset($groups);
        
        $this->groupsProcessed += $groupsProcessed;

        return true;
    }
    
    protected function sanitizeText($text)
    {
        return trim(htmlspecialchars_decode($text));
    }
    
    protected function meetupCurlCall($offset = 0)
    {
        $baseUrl = "http://www.meetup.com/cities/us/{$this->zip}/";
        $curlUrl = $baseUrl . '?' . http_build_query($this->meetupCurlParams);
        
        $this->log("CURL REQUEST: {$curlUrl}");
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);        
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    
    protected function analyzeSinglePageDom(SimpleHtmlDom &$SimpleHtmlDom, $curlResult)
    {
        $this->log("ANALYZING DOM");
        $SimpleHtmlDom->load($curlResult);        
        $groups = $SimpleHtmlDom->find('.D_group');
        $forwardPaginator = $SimpleHtmlDom->find('.resultset_last_icon');
        
        if (empty($forwardPaginator)) {
            $this->lastPage = true;
            $this->log("FINAL PAGE");
        }
        
        if (empty($groups)) {
            $this->log("Found no result at offset: {$params['offset']}.");
            return false;
        }

        $groupCount = 0;
        $groupsData = array();
        foreach($groups as $group) {
            $groupsData[$groupCount] = array();
            
            $GroupDom = new SimpleHtmlDom;
            $GroupDom->load($group->outertext);                                
            //$this->log($groupListing->outertext);

            foreach($GroupDom->find('.D_name') as $title) {                    
                $groupsData[$groupCount]['GroupTitle'] = $this->sanitizeText($title->plaintext);
                break;
            }

            foreach($GroupDom->find('a') as $anchor) {                    
                $groupsData[$groupCount]['Url'] = $anchor->href;
                break;
            }

            foreach($GroupDom->find('.D_distance') as $locationInfo) {
                $distance = $this->sanitizeText($locationInfo->children(0)->plaintext);
                $distance = preg_replace('/[^0-9\.]/', '', $distance);
                $groupsData[$groupCount]['DistanceA'] = $distance;
                
                $location = $this->sanitizeText($locationInfo->children(1)->plaintext);
                $location = substr($location, strpos($location, ' ') + 1);
                $groupsData[$groupCount]['Location'] = $location;
                break;
            }

            foreach($GroupDom->find('.D_members') as $membershipInfo) {
                $trimmedMemberCount = $this->sanitizeText($membershipInfo->children(0)->plaintext);
                $groupsData[$groupCount]['MemberCount'] = $trimmedMemberCount;
                $this->log("MEMBER COUNT: {$trimmedMemberCount}");
                break;
            }

            foreach($GroupDom->find('.meetup-topic') as $topic) {
                $groupsData[$groupCount]['Topics'][] = $this->sanitizeText($topic->plaintext);
            }
            
            unset($GroupDom); //this fixed a memory leak
            $groupCount++;
        }
        
        $this->groupsSaved += count($groupsData);
        return $groupsData;
    }
    
    protected function savePageDom(\Doctrine\ODM\MongoDB\DocumentManager &$dm, &$groups)
    {        
        $this->log("PERSISTING PAGE TO DB");
        foreach($groups as $group) {
            $Group_Doc = $dm->getRepository('CrawlerMeetupBundle:Group')
                    ->findOneBy(array('url' => $group['Url']));

            //$this->log(var_dump($Group_Doc)); //testing

                
            $persistGroup = false; //doctrine persistence required for new document
            
            if (!$Group_Doc) {
                $Group_Doc = new Group();
                $persistGroup = true;
                //$this->log("Group: Creating {$group['GroupTitle']}");                            
            } else {
                //$this->log("Group: Updating {$Group_Doc->getUrl()}");            
            }
            
            foreach($group as $key=>$value) {
                $method = 'set' . $key;
                if (is_string($value))
                    $Group_Doc->$method(utf8_encode($value));

                if ($persistGroup)
                    $dm->persist($Group_Doc); 
            } 
            
            //$this->log(var_dump($group)); //testing
            
            foreach($group['Topics'] as $topic) {
                $topic = utf8_encode($topic);
                $persistTopic = false;

                $Group_X_Topic_Doc = $dm->getRepository('CrawlerMeetupBundle:Group_X_Topic')
                        ->findOneBy(array('url' => $group['Url'], 'topic' => $topic));

                if (!$Group_X_Topic_Doc) {
                    $Group_X_Topic_Doc = new Group_X_Topic();
                    $persistTopic = true;      
                }

                $Group_X_Topic_Doc->setTopic($topic);
                $Group_X_Topic_Doc->setGroup($Group_Doc);

                if ($persistTopic)
                    $dm->persist($Group_X_Topic_Doc);
                
                $Group_Doc->addTopics($Group_X_Topic_Doc);
                
                unset($Group_X_Topic_Doc);
            }
            unset($Group_Doc);
        }
        
        $dm->flush(); //doctrine's save function
        return count($groups);
    }
}
?>
