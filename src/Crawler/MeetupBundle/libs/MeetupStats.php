<?php

class MeetupStats {
    protected function compilePageStats(&$groups)
    {
        $this->log("COMPILING PAGE STATS");
        
        $this->stats['totalGroups'] += count($groups);
        
        $groupCount = 0;
        foreach($groups as $group) {
            $groupCount++;
            if (empty($this->stats['avgMembers']['_all']))
                $this->stats['avgMembers']['_all'] = $group['MemberCount'];
            else
                $this->stats['avgMembers']['_all'] = 
                    (($this->stats['avgMembers']['_all'] *  $this->stats['frequency']['_all']) + $group['MemberCount']) / 
                    ($this->stats['frequency']['_all'] + 1);
            $this->stats['frequency']['_all']++;
            
            foreach($group['Topics'] as $topic) {
                if (empty($this->stats['avgMembers'][$topic])) {
                    $this->stats['avgMembers'][$topic] = $group['MemberCount'];
                    $this->stats['frequency'][$topic] = 1;
                } else {
                    $this->stats['avgMembers'][$topic] = 
                        (($this->stats['avgMembers'][$topic] *  $this->stats['frequency'][$topic]) + $group['MemberCount']) / 
                        ($this->stats['frequency'][$topic] + 1);
                    $this->stats['frequency'][$topic]++;
                }
            }
        }
        
        return $groupCount;
    }
    
    public function aggregateStats()
    {
        $this->log("AGGREGATING AND SORTING STATS");        
        $topResults = array(
            'avgMembers' => array(),
            'frequency' => array()
        );
        
        $this->log("Execution Time: {$this->scriptDuration} seconds");
        $printOut0 = "\nAverage Membership for All Groups in Location\n";
        $printOut0 .= "=====================================\n";
        $printOut0 .= "{$this->stats['avgMembers']['_all']}\n";
        
        $printOut0 .= "\nQty of Groups in Location\n";
        $printOut0 .= "=====================================\n";
        $printOut0 .= "{$this->stats['frequency']['_all']}\n";
        $this->log($printOut0);
        
        arsort($this->stats['avgMembers']);
        
        $count = 0;       
        $printOut1 = "\nTop " . self::MAX_RESULTS . ": Average Members by Topic\n";
        $printOut1 .= "=====================================\n";
        foreach($this->stats['avgMembers'] as $topic=>$avgMembers) {
            if (++$count > self::MAX_RESULTS)
                break;
            $printOut1 .= "{$topic}: {$avgMembers} members (Frequency {$this->stats['frequency'][$topic]})\n";
        }
        $this->log($printOut1);
        
        arsort($this->stats['frequency']);

        $count = 0;
        $printOut2 = "\nTop " . self::MAX_RESULTS . ": Frequency by Topic\n";
        $printOut2 .= "=====================================\n";
        
        foreach($this->stats['frequency'] as $topic=>$frequency) {
            if (++$count > self::MAX_RESULTS + 1)
                break;
            if ($count > 1) //ignoring count of "all" groups
                $printOut2 .= "{$topic}: {$frequency} frequency (Avg Members {$this->stats['avgMembers'][$topic]})\n";
        }
        $this->log($printOut2);
        
        $emailBody = "{$printOut0}\n\n\n{$printOut1}\n\n\n{$printOut2}";        
        $this->emailToAdmin($emailBody);
    }
}
?>